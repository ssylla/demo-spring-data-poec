package fr.epsi.poec.demospring.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_PERSON")
public class Person implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "NOM_COMPLET")
	private String name;
	private String twitter;
	
	public Person() {
	}
	
	public Person( String name, String twitter ) {
		this.name = name;
		this.twitter = twitter;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName( String name ) {
		this.name = name;
	}
	
	public String getTwitter() {
		return twitter;
	}
	
	public void setTwitter( String twitter ) {
		this.twitter = twitter;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Person{" );
		sb.append( "id=" ).append( id );
		sb.append( ", name='" ).append( name ).append( '\'' );
		sb.append( ", twitter='" ).append( twitter ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
