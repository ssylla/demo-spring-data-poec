package fr.epsi.poec.demospring.dal;

import fr.epsi.poec.demospring.domain.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(path = "mesclients", rel = "personsList")
public interface IPersonDAO extends CrudRepository<Person, Long> {
	
	
	@RestResource(path = "twitter")
	Person findByTwitter(@Param( "id" ) String twiter );
	Person findByTwitterAndName(String twiter, String name);
	
	
	
}
