package fr.epsi.poec.demospring.dal;

import fr.epsi.poec.demospring.domain.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IPersonDAOTest {
	
	@Autowired
	IPersonDAO dao;
	
	@Test
	public void save(){
		//Initialisation
		Person person = new Person( "Sega", "ssylla" );
		
		//Action
		dao.save( person );
		
		//Test
		assertNotNull( person.getId() );
	}
}